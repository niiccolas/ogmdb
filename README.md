# OGMDb 😎🎟🍿🤙💎

[https://ogmdb.netlify.app/](https://ogmdb.netlify.app/)

> *Movie must-sees from OGs in the biz*

La OG Movie Database est une application React.js qui affiche un catalogue web de films recommandés par des légendes du cinéma.

Le contenu est obtenu depuis l'API exposée par [The Movie Database](https://www.themoviedb.org/).

## Lazy & adaptatif

> According to HTTP Archive, a typical mobile web page weighs over 2.6 MB, and more than two thirds of that weight is images.
> source: [Top tips for web performance](https://web.dev/use-srcset-to-automatically-choose-the-right-image/)

Si c'est une réalité générale pour le web, sur un projet de **catalogue** **cinéma** une grande partie des échanges de données sera occupée par les requêtes de média. J'oriente en conséquence mes premiers effort de développement pour répondre à ce besoin avec l'implémentation :

- d'un composant React `ResponsiveImage` permettant de générer des [images adaptatives](https://developer.mozilla.org/fr/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images) servant à l'utilisateur la ressource exacte pour son équipement.
- d'un custom hook `useVisible` basé sur l'API [Intersection Observer](https://developer.mozilla.org/fr/docs/Web/API/Intersection_Observer_API) permettant de requêter les ressources à la demande ("lazy-loading") et non de façon monolithique et indistincte.

Si des packages disponibles implémentent déjà ces fonctionnalités, implémenter les miens me permet de mieux comprendre les outils que j'utilise au quotidien.

## Outillé

Le délai de réalisation du projet étant limité, aux boilerplates de config perso je préfère la rapidité de `create-react-app`. Je choisis [React Testing Library](https://testing-library.com/) comme utilitaire de test aux côtés de Jest, pour sa facilité de configuration, d'utilisation et son approche centrée utilisateur dans laquelle je me retrouve.

Pour profiter de l'intellisense TypeScript et d'un typage cléf en mains, j'utilise la librairie [moviedb-promise](https://www.npmjs.com/package/moviedb-promise) pour le requêtage vers TMDB. Elle expose autant de méthodes qu'il y a d'endpoints TMDB, le tout typé, paramètres et retours.

## Choix techniques

Les composants sont conçus dans une approche de séparation des responsabilités. Si certains composants embarquent une logique plus fournie, celle-ci est isolée dans un fichier `utils.js` ou un sous-composant. Cela ouvre aussi à des tests plus robustes et simples à maintenir.

Dans la même idée cette idée, et pour éviter la répétition de code, j'abstrais la librairie moviedb-promise dans un hook custom `useTmdbFetch` gérat les états de requêtage. De même, côté UI, un seul composant `FetchStatusUI` permet de refléter ces changements d'états à l'utilisateur.

Chaque composant est testé de façon unitaire, autant pour prévenir les non régressions que pour documenter leur fonctionnement attendu. Certains choix techniques sont documentés directement dans le code (ex: justifier l'utilisation d'un `useMemo`). Autant que possible, les API de mes composants et utilitaires sont décrites via JSDOC. De façon plus meta, j'utilise l'abstraction du "design atomique" pour rationaliser l'arborescence du projet.

Enfin, pour le création des styles, j'utilise SASS pour son nesting, ses variables et mixins : avec un investissement de temps raisonnable en début de projet, l'outil permet de gagner du temps dans la création d'UI.

## Bonus

OGMDb permet d'afficher le détails de tous les films présents sur la page d'acceuil. N'ayant pas identifié de besoin SEO et s'agissant d'un simple affichage client, la modale m'a semblé être le choix technique le plus pertinent. Elle permet aussi une expérience plus immédiate et immersive pour l'utilisateur, augmentant son engagement.

## Bonus*2

Cinéphile et ravi de mes nouveaux hooks, je n'ai pas résisté à la tentation de les mettre à profit en développant une bonus feature de dernière minute, un composant de recherche à scroll infini. Un POC perso, pas encore testé, ni refactorisé, accessible façon "easter egg" dans le coin bas gauche de la page, mais qui permet d'ouvrir vers une v2 de ce projet :)
