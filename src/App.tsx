import MovieFinder from './components/organisms/MovieFinder/MovieFinder';
import MovieCollection from './components/organisms/MovieCollection/MovieCollection';
import { MOCK_CMS } from './utils';

import './App.scss';

const App = () => {
  return (
    <>
      {MOCK_CMS.slices.MOVIE_COLLECTIONS.map((collection) => (
        <MovieCollection key={collection.params.id} {...collection} />
      ))}
      <MovieFinder />
    </>
  );
};

export default App;
