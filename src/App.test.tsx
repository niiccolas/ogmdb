import { render } from '@testing-library/react';
import App from './App';

jest.mock(
  './components/organisms/MovieFinder/MovieFinder',
  () => 'mock-movie-finder'
);
jest.mock(
  './components/organisms/MovieCollection/MovieCollection',
  () => 'mock-movie-collection'
);

it('should render correctly the main app components', () => {
  const { container } = render(<App />);
  expect(container).toMatchSnapshot();
});
