import { render } from '@testing-library/react';

import AvatarList, { AvatarListProps } from './AvatarList';

jest.mock('../../atoms/Badge/Badge', () => 'mock-badge');

const props: AvatarListProps = {
  avatars: [
    {
      character: 'Caleb Smith',
      name: 'Domhnall Gleeson',
      popularity: 19.491,
      src: '/img1.jpg',
    },
    {
      character: 'char2',
      name: 'name',
      popularity: 2,
      src: '/img2.jpg',
    },
  ],
  isStacked: true,
  showTooltip: true,
  size: 'small',
  className: 'className',
  title: 'title',
};

describe('BadgeList', () => {
  it('should not render without props', () => {
    const { container } = render(<AvatarList />);
    expect(container.firstChild).toBeNull();
  });

  it('should correctly with all props', () => {
    const { container } = render(<AvatarList {...props} />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
