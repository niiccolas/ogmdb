import classnames from 'classnames';

import Avatar from '../../atoms/Avatar/Avatar';

import './AvatarList.scss';

export type AvatarType = {
  src?: string;
  name?: string;
  character?: string;
  popularity?: number;
};

export type AvatarListProps = {
  avatars?: Array<AvatarType>;
  isStacked?: boolean; // Display as a stack of overlapping images
  showTooltip?: boolean; // Display tooltip on hover
  size?: 'small' | 'large'; // Quick override for size
  className?: string;
  title?: string;
};

/**
 * Displays a list of Avatar items
 */
const AvatarList = ({
  title,
  avatars,
  isStacked = true,
  showTooltip,
  size,
  className,
}: AvatarListProps) =>
  !avatars || avatars.length === 0 ? null : (
    <div className={classnames('AvatarList', className)}>
      {title && <p className="AvatarList__title">{title}</p>}
      <ul
        className={classnames('AvatarList__items', {
          'is-stacked': isStacked,
        })}
      >
        {avatars.map((avatar) => (
          <li
            className={classnames('AvatarList__item', {
              'AvatarList__item--stacked': isStacked,
            })}
            key={avatar.name}
          >
            <Avatar
              label={avatar.name}
              subLabel={avatar.character}
              imageUrl={avatar.src}
              showTooltip={showTooltip}
              key={avatar.name}
              size={size}
            />
          </li>
        ))}
      </ul>
    </div>
  );

export default AvatarList;
