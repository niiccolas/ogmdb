import { Browser, Ghost } from 'react-kawaii';
import classnames from 'classnames';

import { MOCK_CMS } from '../../../utils';

import Spinner from '../../atoms/Spinner/Spinner';

import './FetchStatusUI.scss';

export type FetchStatusUIProps = {
  showLoader?: boolean;
  showNoData?: boolean;
  showError?: boolean;
  isCenteredXy?: boolean;
};

/**
 * A React component that displays a spinner, error message, or no data message
 */
const FetchStatusUI = ({
  showLoader = false,
  showNoData = false,
  showError,
  isCenteredXy = false,
}: FetchStatusUIProps) => {
  if (showLoader)
    return (
      <Spinner
        className={classnames('Spinner', {
          'Spinner--xy-center-top': isCenteredXy,
        })}
      />
    );

  const SvgIcon = showError ? Browser : Ghost;
  if (showError || showNoData)
    return (
      <div className="FetchStatusUI__problem">
        <SvgIcon size={150} mood="sad" />
        <p>
          {showError
            ? MOCK_CMS.i18n.errGenericLabel
            : MOCK_CMS.i18n.noDataLabel}
        </p>
      </div>
    );

  return null;
};

export default FetchStatusUI;
