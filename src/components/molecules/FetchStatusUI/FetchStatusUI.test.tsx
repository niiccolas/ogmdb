import { render } from '@testing-library/react';
import FetchStatusUI from './FetchStatusUI';

jest.mock('react-kawaii', () => ({
  __esModule: true,
  Browser: () => <svg>mock-BrowserSvg-react-kawaii</svg>,
  Ghost: () => <svg>mock-GhostSvg-react-kawaii</svg>,
}));
jest.mock('../../atoms/Spinner/Spinner', () => 'mock-spinner');

describe('FetchStatusUI', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should not render without props', () => {
    const { container } = render(<FetchStatusUI />);
    expect(container.firstChild).toBeNull();
  });

  it('should render correctly when loading', () => {
    const { container } = render(<FetchStatusUI showLoader />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('should render correctly when it receives an error', () => {
    const { container } = render(<FetchStatusUI showError />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('should render correctly when it receives no data', () => {
    const { container } = render(<FetchStatusUI showNoData />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
