import classnames from 'classnames';

import Badge, { BadgeType } from '../../atoms/Badge/Badge';

import './BadgeList.scss';

export type BadgeListProps = {
  badges?: BadgeType[];
  className?: string;
};

/**
 * BadgeList component.
 */
const BadgeList = ({ badges, className }: BadgeListProps) =>
  badges && badges.length ? (
    <ul className={classnames('BadgeList', className)}>
      {badges?.map((badge) =>
        badge.label ? (
          <li className="BadgeList__item" key={badge.label}>
            <Badge {...badge} />
          </li>
        ) : null
      )}
    </ul>
  ) : null;

export default BadgeList;
