import { render } from '@testing-library/react';

import BadgeList, { BadgeListProps } from './BadgeList';

jest.mock('../../atoms/Badge/Badge', () => 'mock-badge');

const props: BadgeListProps = {
  badges: [
    {
      label: 'Akira Kurosawa',
      isPrimary: true,
      isHighlight: true,
    },
    {
      label: 'Action',
      isCapsule: true,
      isHighlight: true,
    },
    { label: '3h27' },
    { label: 1954 },
    { label: 'Japanese' },
  ],
  className: 'prop-classname',
};

describe('BadgeList', () => {
  it('should not render without props', () => {
    const { container } = render(<BadgeList />);
    expect(container.firstChild).toBeNull();
  });

  it('should correctly with all props', () => {
    const { container } = render(<BadgeList {...props} />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
