import { useCallback, forwardRef, useState, useEffect } from 'react';
import { MovieResult } from 'moviedb-promise/dist/request-types';
import classnames from 'classnames';

import ResponsiveImage from '../../atoms/ResponsiveImage/ResponsiveImage';

import MovieDetails from '../../organisms/MovieDetails/MovieDetails';

import './Movie.scss';

export type MovieProps = {
  displayMode: 'backdrop' | 'poster';
} & MovieResult;

/**
 * Displays invidiual movie specs: title, poster, backdrop image. Holds the `ref` passed  from parent to handle IntersectionObserver events.
 */
const Movie = forwardRef(
  (
    {
      title,
      poster_path,
      backdrop_path,
      original_title,
      id,
      displayMode,
      overview,
    }: MovieProps,
    ref: any
  ) => {
    const [isOpen, setIsOpen] = useState(false);
    const hasMovieImage = !!poster_path || !!backdrop_path;
    const isBackdrop = displayMode === 'backdrop';

    /**
     * Toggle CSS class on body to prevent unwanted scrolling when movie details modal is open
     */
    useEffect(() => {
      document.querySelector('#root')?.classList.toggle('modal-open', isOpen);
    }, [isOpen]);

    /**
     * Memoized with useCallback to send a stable function identity to
     * the child component's useEffect deps array
     */
    const handleCloseModal = useCallback(() => setIsOpen(false), []);

    const handleOpenModal = () => setIsOpen(true);

    return (
      <li
        className={classnames('Movie', {
          'is-backdrop': isBackdrop,
        })}
        ref={ref}
      >
        <div
          className="Movie__wrapper"
          onClick={handleOpenModal}
          role="button"
          aria-label={`Movie card for: ${title}`}
        >
          {isBackdrop && (
            <p className="Movie__title">
              {title}
              {title !== original_title && (
                <span className="Movie__titleOriginal">{original_title}</span>
              )}
            </p>
          )}
          {hasMovieImage && (
            <ResponsiveImage
              backdropPath={backdrop_path}
              posterPath={poster_path}
              alt={title}
              displayMode={displayMode}
            />
          )}
        </div>
        {id !== undefined && (
          <MovieDetails
            showModal={isOpen}
            onClose={handleCloseModal}
            movieId={id}
          >
            {id}
            {title && <h2>{title}</h2>}
            {overview && <p>{overview}</p>}
          </MovieDetails>
        )}
      </li>
    );
  }
);

export default Movie;
