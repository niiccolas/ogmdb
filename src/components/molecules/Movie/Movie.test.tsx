import { render, fireEvent, screen } from '@testing-library/react';

import Movie, { MovieProps } from './Movie';

jest.mock(
  '../../organisms/MovieDetails/MovieDetails',
  () => 'mock-movie-details'
);
jest.mock(
  '../../atoms/ResponsiveImage/ResponsiveImage',
  () => 'mock-responsive-image'
);

document.body.innerHTML = '<div id="root"></div>';

const props: MovieProps = {
  title: 'Force Majeure',
  original_title: 'Turist',
  poster_path: 'https://poster.jpg',
  backdrop_path: 'https://backdrop.jpg',
  id: 278,
  displayMode: 'backdrop',
  overview: 'While holidaying in the French Alps...',
  media_type: 'movie',
};

describe('Movie', () => {
  it('should render correctly without props', () => {
    // @ts-ignore
    const { container } = render(<Movie />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('should render correctly with props', () => {
    const { container } = render(<Movie {...props} />);
    expect(container.firstChild).toMatchSnapshot();
  });

  describe('Interactions', () => {
    it('should toggle modal-open CSS class on #root when Movie is clicked and modal opens', () => {
      render(<Movie {...props} />);
      fireEvent.click(screen.getByRole('button'));

      expect(
        document.querySelector('#root')?.classList.contains('modal-open')
      ).toBeTruthy();
    });
  });
});
