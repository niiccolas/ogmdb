import { useEffect, useRef, useReducer, useId } from 'react';
import useVisible from '../../../utils/hooks/useVisible';
import useIsMounted from '../../../utils/hooks/useIsMounted';
import { isValidQuery, TMDB } from '../../../utils';
import { tmdb } from '../../../utils/hooks/useTmdbFetch';

import Movie from '../../molecules/Movie/Movie';
import FetchStatusUi from '../../molecules/FetchStatusUI/FetchStatusUI';
import Button from '../../atoms/Button/Button';

import './MovieFinder.scss';

import {
  MovieResult,
  MovieResultsResponse,
} from 'moviedb-promise/dist/request-types';

enum SearchAction {
  START = 'START',
  NEXT_PAGE = 'NEXT_PAGE',
  NEW_QUERY = 'NEW_QUERY',
  ERROR = 'ERROR',
  SUCCESS = 'SUCCESS',
}

type State = {
  query?: string;
  results: MovieResult[];
  page: number;
  totalPages?: number;
  totalResults?: number;
  isSearching: boolean;
  hasError: boolean;
};

const initialState: State = {
  results: [],
  page: 1,
  isSearching: false,
  hasError: false,
};

type Action = {
  type: SearchAction;
  payload?: { query?: string } & MovieResultsResponse;
};

/**
 * Reducer to handle the logic for querying TMDB for a movie and paginating that results based on scroll (infinite scroll)
 */
const searchReducer = (state: State, { type, payload }: Action): State => {
  switch (type) {
    case SearchAction.START:
      return { ...state, isSearching: true, hasError: false };
    case SearchAction.ERROR:
      return { ...state, isSearching: false, hasError: true };
    case SearchAction.NEXT_PAGE:
      return { ...state, isSearching: true, page: state.page + 1 };
    case SearchAction.NEW_QUERY: // Reset to initial state on new input queries
      return { ...initialState, query: payload!.query };
    case SearchAction.SUCCESS:
      const { results = [] } = payload || {};
      return {
        ...state,
        isSearching: false,
        page: payload?.page || state.page,
        results: [
          ...state.results,
          /* Besides hiding API Keys, here's another job that could be done by a BFF:
             Combing the raw API results to our own data standards, according to which
             a valid MovieResult item should have a poster and a backdrop image */
          ...results.filter(
            ({ poster_path, backdrop_path }) => !!poster_path && !!backdrop_path
          ),
        ],
        totalPages: payload?.total_pages,
        totalResults: payload?.total_results,
      };

    default:
      return state;
  }
};

/**
 * Displays a list of movie results for the query submitted by the user.
 * Results are paginated and can be scrolled down to query for the next page & set of results.
 *
 */
const MovieFinder = () => {
  // TODO  refactoring with useTmdbFetch + unit tests
  const id = useId();
  const isMounted = useIsMounted();
  const [
    { query, results, page, isSearching, hasError, totalPages, totalResults },
    dispatch,
  ] = useReducer(searchReducer, initialState);

  const { ref: visibilityRef, isVisible: isLastMovieItemVisible } =
    useVisible();
  const inputRef = useRef<HTMLInputElement>(null);
  const listStartRef = useRef<HTMLUListElement>(null);
  const previousQuery = useRef<string>();

  /**
   * Searches a new query of paginates the current one
   */
  useEffect(() => {
    if (query !== undefined && isValidQuery(query)) {
      dispatch({ type: SearchAction.START });
      tmdb
        .searchMovie({ query, page })
        .then((response) => {
          if (isMounted) {
            dispatch({ type: SearchAction.SUCCESS, payload: response });
            previousQuery.current = query; // Keep track of last successful query to condition infinite scroll & input submit
          }
        })
        .catch(() => {
          if (isMounted.current) dispatch({ type: SearchAction.ERROR });
        });
    }
  }, [query, page, isMounted]);

  /**
   * Infinite scroll: increments page number in state when last Movie element appears on screen.
   */
  useEffect(() => {
    if (!isSearching && isLastMovieItemVisible && page < (totalPages || 0)) {
      dispatch({ type: SearchAction.NEXT_PAGE });
    }
  }, [page, totalPages, isSearching, isLastMovieItemVisible]);

  /**
   * Focuses and selects the input field text content. Input is set as a Ref to avoid rerenders
   */
  const selectInput = () => {
    inputRef.current?.select();
    if (inputRef.current) inputRef.current.value = '';
    listStartRef.current?.scrollIntoView(true);
  };

  /**
   * Handle/validate query submits
   */
  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault(); // Prevent native browser behavior that would trigger a page refresh
    const newQuery = inputRef?.current?.value;
    if (!isValidQuery(newQuery) || newQuery === previousQuery.current) {
      selectInput();
      return;
    }
    dispatch({ type: SearchAction.NEW_QUERY, payload: { query: newQuery } });
  };

  return (
    <div className="MovieFinder">
      <div className="MovieFinder__search">
        <form role="search" onSubmit={handleSubmit}>
          <label htmlFor={id}>
            <span className="screen-reader-only">Search blog posts</span>
          </label>
          <input
            ref={inputRef}
            minLength={TMDB.SEARCH_QUERY_MIN_LENGTH}
            type="search"
            id={id}
            placeholder="Enter a movie title…"
          />
          <button type="submit">Search</button>
        </form>
      </div>
      {results.length > 0 && (
        <ul ref={listStartRef}>
          {results.map((movie, index) => {
            const lastMovieRef =
              index === results.length - 1 && !isSearching
                ? visibilityRef
                : null;
            return (
              <Movie
                {...movie}
                displayMode="backdrop"
                ref={lastMovieRef}
                key={movie?.id}
              />
            );
          })}
        </ul>
      )}
      <FetchStatusUi
        showLoader={isSearching}
        showError={hasError}
        showNoData={totalResults === 0}
      />
      {page === totalPages && isLastMovieItemVisible && (
        <footer className="MovieFinder__footer">
          <Button
            isPrimary
            label="Try a new search?"
            type="button"
            onClick={selectInput}
            className="Button--block"
          />
        </footer>
      )}
    </div>
  );
};

export default MovieFinder;
