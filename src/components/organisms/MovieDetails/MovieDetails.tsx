import { ReactNode, useEffect, useMemo } from 'react';
import { createPortal } from 'react-dom';
import useTmdbFetch from '../../../utils/hooks/useTmdbFetch';
import { getMovieCastAvatars, getMovieSpecsBadges } from './utils';
import { MOCK_CMS, TmdbEndpoint } from '../../../utils';

import Button from '../../atoms/Button/Button';
import BadgeList from '../../molecules/BadgeList/BadgeList';
import AvatarList from '../../molecules/AvatarList/AvatarList';
import FetchStatusUi from '../../molecules/FetchStatusUI/FetchStatusUI';
import MovieDetailsHeader from '../MovieDetailsHeader/MovieDetailsHeader';

import './MovieDetails.scss';

import type {
  CreditsResponse,
  MovieResponse,
  VideosResponse,
} from 'moviedb-promise/dist/request-types';

export type MovieDetailsState = MovieResponse & {
  videos?: VideosResponse;
  credits?: CreditsResponse;
};

export type MovieDetailsProps = {
  movieId: number;
  showModal?: boolean;
  onClose: () => void;
  children?: ReactNode;
};

/**
 * A React Portal modal component that displays a backdrop/trailer and general details for given movie ID
 */
const MovieDetails = ({ movieId, showModal, onClose }: MovieDetailsProps) => {
  const portalDomElement = document.getElementById('portal');
  const { data, error, isLoading } = useTmdbFetch({
    endpoint: TmdbEndpoint.MOVIE_INFO,
    params: {
      id: movieId,
      append_to_response: 'similar,videos,reviews,credits,crew',
    },
    shouldFetch: showModal,
  }) as { data: MovieDetailsState; error: Error; isLoading: boolean };
  const {
    credits,
    overview,
    title,
    tagline,
    videos,
    backdrop_path: backdropPath,
  } = data || {};

  const movieSpecsBadges = getMovieSpecsBadges(data);
  const avatars = useMemo(
    // Memoized given the non-negligible computing cost of modeling Avatar objects
    () => getMovieCastAvatars(credits?.cast),
    [credits?.cast]
  );

  /**
   * Close the modal on ESC key down
   */
  useEffect(() => {
    const handleEscKeyDown = (e: KeyboardEvent) => {
      if (e.defaultPrevented) return;
      if (['Escape', 'Esc', 27].includes(e.key || e.keyCode)) onClose();
    };

    // Add listener only when the modal is open
    if (showModal) document.body.addEventListener('keydown', handleEscKeyDown);
    return () => {
      document.body.removeEventListener('keydown', handleEscKeyDown);
    };
  }, [onClose, showModal]);

  return portalDomElement && showModal
    ? createPortal(
        <>
          <FetchStatusUi
            showLoader={isLoading}
            showError={!!error}
            isCenteredXy
          />
          {!!data && (
            <div className="MovieDetails">
              <MovieDetailsHeader
                title={title}
                backdropPath={backdropPath}
                videos={videos}
              />
              <main className="MovieDetails__main">
                {title && <p className="MovieDetails__title">{title}</p>}
                {tagline && <p className="MovieDetails__tagline">{tagline}</p>}
                <BadgeList badges={movieSpecsBadges} />
                <div className="MovieDetails__overviewWrapper">
                  {overview && (
                    <p className="MovieDetails__overview">{overview}</p>
                  )}
                  <div className="MovieDetails__cast">
                    <AvatarList avatars={avatars} isStacked />
                    <Button
                      label={MOCK_CMS.i18n.returnList}
                      onClick={() => onClose()}
                      size="small"
                    />
                  </div>
                </div>
              </main>
            </div>
          )}
          <div className="MovieDetails__overlay" onClick={() => onClose()} />
        </>,
        portalDomElement
      )
    : null;
};

export default MovieDetails;
