import { parseToHoursMinutes, TMDB } from '../../../utils';

import { Cast } from 'moviedb-promise/dist/request-types';
import { BadgeType } from '../../atoms/Badge/Badge';
import type { AvatarType } from '../../molecules/AvatarList/AvatarList';
import type { MovieDetailsState } from './MovieDetails';

/**
 * Returns an array of modelled Badge objects datafed from the movie details
 */
export const getMovieSpecsBadges = ({
  original_language,
  release_date,
  spoken_languages,
  runtime,
  credits,
  genres,
}: MovieDetailsState = {}): BadgeType[] => {
  const directorName = credits?.crew?.find(
    ({ job }) => job === 'Director'
  )?.name;
  const firstGenre = genres?.[0]?.name;
  const runtimeHoursMin = parseToHoursMinutes(runtime);
  const releaseYear = new Date(release_date || '').getFullYear();
  const originalLangue = spoken_languages?.find(
    (lang) => lang.iso_639_1 === original_language
    // !TODO: send pr to update moviedb-promise typing for SpokenLanguage[] type
    // @ts-ignore:
  )?.english_name;

  return [
    {
      label: directorName,
      isPrimary: true,
      isHighlight: true,
      onClick: () => alert(directorName),
    },
    {
      label: firstGenre,
      isCapsule: true,
      isHighlight: true,
    },
    { label: runtimeHoursMin },
    { label: releaseYear },
    {
      label: originalLangue,
    },
  ];
};

/**
 * Returns an array of modelled Avatar objects datafed from the movie details
 */
export const getMovieCastAvatars = (cast?: Cast[]) => {
  if (cast === undefined) return undefined;

  const castByAscPopularity = [...cast].sort((a, b) => {
    return (b?.popularity || 0) - (a?.popularity || 0);
  });
  return castByAscPopularity.reduce(
    (
      acc: AvatarType[],
      { name, character, profile_path, popularity = 0 }: Cast
    ) => {
      // Ignore cast members with no profile picture
      if (!profile_path) return acc;
      // Model data to the Avatar type
      const avatar: AvatarType = {
        name,
        character,
        src: profile_path
          ? TMDB.IMG.baseURL + TMDB.IMG.width.profile.w185 + profile_path
          : undefined,
        popularity,
      };
      // Sort from most to least popular
      return popularity > (acc[acc.length - 1]?.popularity || 0)
        ? [avatar, ...acc]
        : [...acc, avatar];
    },
    []
  );
};
