import ReactDOM from 'react-dom';
import { render, fireEvent } from '@testing-library/react';

import MovieDetails, { MovieDetailsProps } from './MovieDetails';

jest.mock('../../atoms/Button/Button', () => 'mock-button');
jest.mock('../../molecules/BadgeList/BadgeList', () => 'mock-badge-list');
jest.mock('../../molecules/AvatarList/AvatarList', () => 'mock-avatar-list');
jest.mock(
  '../../organisms/MovieDetailsHeader/MovieDetailsHeader',
  () => 'mock-movie-details-header'
);

jest.mock(
  '../../molecules/FetchStatusUI/FetchStatusUI',
  () => 'mock-fetch-status-ui'
);

const mockOnClose = jest.fn();
const mockUseFetch = jest.fn();
jest.mock('../../../utils/hooks/useTmdbFetch', () => () => mockUseFetch());

const portalDomElement = document.createElement('div');
portalDomElement.setAttribute('id', 'portal');
document.body.appendChild(portalDomElement);

const props: MovieDetailsProps = {
  movieId: 123,
  showModal: true,
  onClose: mockOnClose,
};

describe('MovieDetails', () => {
  beforeEach(() => {
    jest.clearAllMocks();
    mockUseFetch.mockReturnValue({
      data: {},
      error: undefined,
      isLoading: false,
    });
    // @ts-ignore
    ReactDOM.createPortal = jest.fn((element) => {
      return element;
    });
  });

  afterEach(() => {
    // @ts-ignore
    ReactDOM.createPortal.mockClear();
  });

  describe('With an API response', () => {
    it('should render loader when API call is loading', () => {
      mockUseFetch.mockReturnValueOnce({ isLoading: true });
      const { container } = render(<MovieDetails {...props} />);
      expect(container.firstChild).toMatchInlineSnapshot(`
        <mock-fetch-status-ui
          iscenteredxy="true"
          showerror="false"
          showloader="true"
        />
      `);
    });

    it('should render error when API call failed', () => {
      mockUseFetch.mockReturnValueOnce({ error: Error('big problem') });
      const { container } = render(<MovieDetails {...props} />);
      expect(container.firstChild).toMatchInlineSnapshot(`
        <mock-fetch-status-ui
          iscenteredxy="true"
          showerror="true"
        />
      `);
    });

    it('should render correctly when API responds with data', () => {
      mockUseFetch.mockReturnValueOnce({
        data: {
          overview: 'overview',
          title: 'title',
          tagline: 'tagline',
          backdrop_path: 'backdrop_path',
          videos: { k: 'v' },
          credits: {
            cast: [
              {
                name: 'Domhnall Gleeson',
                popularity: 16.97,
                profile_path: '/123.jpg',
                character: 'Caleb Smith',
              },
            ],
          },
        },
      });
      const { container } = render(<MovieDetails {...props} />);
      expect(container).toMatchSnapshot();
    });

    describe('Interactions', () => {
      it('should trigger onClose() when button is clicked', () => {
        const { container } = render(<MovieDetails {...props} />);
        fireEvent.keyDown(container, {
          key: 'Escape',
        });
        expect(mockOnClose).toHaveBeenCalledTimes(1);
      });
    });
  });
});
