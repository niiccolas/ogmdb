import { render, screen, fireEvent } from '@testing-library/react';
import cloneDeep from 'lodash.clonedeep';

import MovieDetailsHeader, {
  MovieDetailsHeaderProps,
} from './MovieDetailsHeader';

jest.mock('react-icons/fa', () => ({
  FaStopCircle: 'mock-svg-FaStopCircle',
  FaPlayCircle: 'mock-svg-FaPlayCircle',
}));
jest.mock('../../atoms/Trailer/Trailer', () => 'mock-trailer');
jest.mock(
  '../../atoms/ResponsiveImage/ResponsiveImage',
  () => 'mock-responsive-image'
);

const props: MovieDetailsHeaderProps = {
  title: 'They Live',
  backdropPath: '/img.jpg',
  videos: {
    results: [{ key: 'xyz', type: 'Trailer', site: 'YouTube' }],
  },
};

describe('MovieDetailsHeader', () => {
  it('should render correctly without props', () => {
    const { container } = render(<MovieDetailsHeader />);
    expect(container).toMatchSnapshot();
  });

  it('should render correctly with all props', () => {
    const { container } = render(<MovieDetailsHeader {...props} />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('should not render Play trailer toggle button when trailer ID is missing', () => {
    const localProps = cloneDeep(props);
    localProps.videos!.results![0].key = undefined;
    const { queryByLabelText } = render(<MovieDetailsHeader {...localProps} />);

    expect(queryByLabelText('Play/Stop trailer')).toBeNull();
  });

  it('should swap backdrop image for video trailer player when Play/Stop toggle button is clicked', () => {
    const { container } = render(<MovieDetailsHeader {...props} />);

    fireEvent.click(screen.getByLabelText('Play/Stop trailer'));
    expect(container.querySelector('mock-trailer')).toBeInTheDocument();

    fireEvent.click(screen.getByLabelText('Play/Stop trailer'));
    expect(container.querySelector('mock-trailer')).not.toBeInTheDocument();
  });
});
