import { useState } from 'react';
import { FaStopCircle, FaPlayCircle } from 'react-icons/fa';
import { VideosResponse } from 'moviedb-promise/dist/request-types';
import classnames from 'classnames';

import Trailer from '../../atoms/Trailer/Trailer';
import ResponsiveImage from '../../atoms/ResponsiveImage/ResponsiveImage';
import { TMDB } from '../../../utils/constants';

import './MovieDetailsHeader.scss';

export type MovieDetailsHeaderProps = {
  videos?: VideosResponse;
  backdropPath?: string;
  title?: string;
};

/**
 * Displays a backdrop image or video trailer as header of a movie details modal
 */
const MovieDetailsHeader = ({
  videos,
  backdropPath,
  title,
}: MovieDetailsHeaderProps) => {
  const [showTrailer, setShowTrailer] = useState<boolean>(false);
  const { key: trailerKey } =
    videos?.results?.find(({ type }) => type === TMDB.VIDEO_TYPE.trailer) || {};
  const PlayToggleBtn = showTrailer ? FaStopCircle : FaPlayCircle;

  return (
    <header className="MovieDetailsHeader">
      {trailerKey && showTrailer ? (
        <Trailer {...videos} />
      ) : (
        <ResponsiveImage
          backdropPath={backdropPath}
          alt={title}
          displayMode="backdrop"
        />
      )}
      {trailerKey && (
        <PlayToggleBtn
          className={classnames('MovieDetailsHeader__controls', {
            'is-showOnFocus': showTrailer,
          })}
          onClick={() => setShowTrailer((prev) => !prev)}
          role="button"
          aria-label="Play/Stop trailer"
        />
      )}
    </header>
  );
};

export default MovieDetailsHeader;
