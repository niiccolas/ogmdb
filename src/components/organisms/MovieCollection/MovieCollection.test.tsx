import { render } from '@testing-library/react';

import MovieCollection, { MovieCollectionProps } from './MovieCollection';
import { TmdbEndpoint } from '../../../utils/constants';

jest.mock('../../molecules/Movie/Movie', () => 'mock-movie');
jest.mock(
  '../../molecules/FetchStatusUI/FetchStatusUI',
  () => 'mock-fetch-status-ui'
);

const mockUseFetch = jest.fn();
jest.mock('../../../utils/hooks/useTmdbFetch', () => () => mockUseFetch());

const props: MovieCollectionProps = {
  collectionName: 'Ava DV.',
  endpoint: TmdbEndpoint.LIST_INFO,
  params: {
    id: 123,
  },
};

describe('MovieCollection', () => {
  beforeEach(() => {
    mockUseFetch.mockReturnValue({
      data: {},
      error: undefined,
      isLoading: false,
    });
  });

  it('should render correctly without props', () => {
    // @ts-ignore
    const { container } = render(<MovieCollection />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('should render correctly with props', () => {
    const { container } = render(<MovieCollection {...props} />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('should render correctly with props and data from API', () => {
    mockUseFetch.mockReturnValueOnce({
      data: {
        name: 'Collection Name from API',
        items: [
          { title: 'Dog Day Afternoon', id: 1 },
          { title: 'Harlan County U.S.A.', id: 2 },
        ],
      },
    });
    const { container } = render(<MovieCollection {...props} />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
