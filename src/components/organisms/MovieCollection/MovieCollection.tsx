import {
  ListsDetailResponse,
  MovieResult,
} from 'moviedb-promise/dist/request-types';

import useTmdbFetch from '../../../utils/hooks/useTmdbFetch';

import Movie from '../../molecules/Movie/Movie';
import FetchStatusUi from '../../molecules/FetchStatusUI/FetchStatusUI';
import { TmdbEndpoint } from '../../../utils/constants';

import './MovieCollection.scss';

export type MovieCollectionProps = {
  collectionName: string; // Fallback collection name from CMS
  endpoint: TmdbEndpoint;
  params: { [key: string]: string | number };
};

/**
 * Displays a collection of movies for a given TMDB endpoint
 */
const MovieCollection = ({
  collectionName = '',
  endpoint,
  params,
}: MovieCollectionProps) => {
  const { error, isLoading, data } = useTmdbFetch({ endpoint, params }) as {
    error: Error;
    isLoading: boolean;
    data: ListsDetailResponse;
  };
  const { name, items: movies } = data || {};
  const collectionTitle = name || collectionName;

  return (
    <div className="MovieCollection">
      <FetchStatusUi showLoader={isLoading} showError={!!error} />
      {collectionTitle && (
        <h3 className="MovieCollection__title">{collectionTitle}</h3>
      )}
      {movies && (
        <ul className="MovieCollection__scroller">
          {movies.map((movie: MovieResult) => (
            <Movie {...movie} displayMode="poster" key={movie?.id} />
          ))}
        </ul>
      )}
    </div>
  );
};

export default MovieCollection;
