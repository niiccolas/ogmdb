import classnames from 'classnames';

import ConditionalWrapper from '../ConditionalWrapper/ConditionalWrapper';

import './Badge.scss';

export type BadgeType = {
  label?: string | number;
  isPrimary?: boolean; // Fills badge's background for more visual emphasis
  isHighlight?: boolean; // Adds highlight color
  isCapsule?: boolean; // Capsule shape (rounded corners)
  link?: string; // Actionnable link URL, overrides onClick!
  onClick?: () => void;
  className?: string;
};

/**
 * A rectangle shaped atom to display and highlight short bits of information. Can receive an optional link or function to make it actionnable.
 *
 * @example <Badge label="John Carpenter" link="https://www.themoviedb.org/person/11770-john-carpenter" isPrimary isHighlight />
 */
const Badge = ({
  label,
  link,
  isPrimary,
  isHighlight,
  isCapsule,
  className,
  onClick,
}: BadgeType) => {
  const isActionnable = !!link || !!onClick;

  if (label === undefined) return null;
  return (
    <ConditionalWrapper
      shouldWrap={isActionnable}
      wrapper={(badge) => (
        <a
          href={link}
          onClick={onClick}
          target="_blank"
          role={link && !onClick ? 'link' : 'button'}
          rel="noreferrer"
        >
          {badge}
        </a>
      )}
    >
      <div
        className={classnames('Badge', className, {
          'Badge--primary': isPrimary,
          'Badge--highlight': isHighlight,
          'Badge--capsule': isCapsule,
          'Badge--actionnable': isActionnable,
        })}
        role="tooltip"
        aria-label={label?.toString()}
      >
        {label}
      </div>
    </ConditionalWrapper>
  );
};

export default Badge;
