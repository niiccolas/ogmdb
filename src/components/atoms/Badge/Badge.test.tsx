import { render, screen, fireEvent } from '@testing-library/react';
import Badge, { BadgeType } from './Badge';

const mockOnClick = jest.fn();

const props: BadgeType = {
  label: 'John Carpenter',
  isPrimary: true,
  isHighlight: true,
  isCapsule: true,
  link: 'https://www.themoviedb.org/person/11770-john-carpenter',
  className: 'prop-classname',
  onClick: mockOnClick,
};

describe('Badge', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should not render when `label` is missing', () => {
    const { container } = render(<Badge />);
    expect(container.firstChild).toBeNull();
  });

  it('should render correctly with props', () => {
    const { container } = render(<Badge {...props} />);
    expect(container.firstChild).toMatchSnapshot();
    expect(mockOnClick).not.toHaveBeenCalled();
  });

  it('should trigger onClick method when Badge is clicked', () => {
    render(<Badge {...props} />);
    fireEvent.click(screen.getByRole('button'));
    expect(mockOnClick).toHaveBeenCalledTimes(1);
  });
});
