export type ConditonalWrapperProps = {
  children: React.ReactElement;
  shouldWrap: boolean;
  wrapper: (children: React.ReactElement) => JSX.Element;
};

const ConditonalWrapper = ({
  shouldWrap,
  wrapper,
  children,
}: ConditonalWrapperProps) => (shouldWrap ? wrapper(children) : children);

export default ConditonalWrapper;
