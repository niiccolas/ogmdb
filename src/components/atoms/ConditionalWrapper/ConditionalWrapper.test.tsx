import { render, screen } from '@testing-library/react';

import ConditionalWrapper, {
  ConditonalWrapperProps,
} from './ConditionalWrapper';

const props: ConditonalWrapperProps = {
  shouldWrap: true,
  children: <span>I could be a clickable link</span>,
  wrapper: (children: React.ReactElement) => (
    <a href="/link.html">{children}</a>
  ),
};

describe('ConditionalWrapper', () => {
  it('should render children in wrapper when wrap condition is truthy', () => {
    const { container } = render(<ConditionalWrapper {...props} />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('should not render children inside wrapper when wrap condition is falsy', () => {
    const localProps = { ...props };
    localProps.shouldWrap = false;
    const { container } = render(<ConditionalWrapper {...localProps} />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
