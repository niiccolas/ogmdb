import { VideosResponse } from 'moviedb-promise/dist/request-types';

import './Trailer.scss';

const VIDEO_EMBED_URL: {
  [key: string]: string;
} = {
  YouTube: 'https://www.youtube.com/embed',
  Vimeo: 'https://vimeo.com',
};

const Trailer = (videos?: VideosResponse) => {
  const {
    key: trailerKey,
    site,
    name,
  } = videos?.results?.find(({ type }) => {
    return type === 'Trailer';
  }) || {};

  return trailerKey !== undefined ? (
    <div className="Trailer">
      <iframe
        aria-label={`Trailer for ${name} played from ${site}`}
        width="100%"
        height="100%"
        src={`${VIDEO_EMBED_URL.YouTube}/${trailerKey}?controls=0&autoplay=1&fs=1`}
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
        title="Video Trailer"
      />
    </div>
  ) : null;
};

export default Trailer;
