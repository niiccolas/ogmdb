import { render, screen, fireEvent } from '@testing-library/react';
import { Video, VideosResponse } from 'moviedb-promise/dist/request-types';

import Trailer from './Trailer';

const videoNotOfTypeTrailer: Video = {
  type: 'Featurette',
  key: 'abc',
  name: 'Ran Featurette',
  site: 'YouTube',
};

const props: VideosResponse = {
  results: [
    { type: 'Trailer', key: 'xyz', name: 'Ran', site: 'YouTube' },
    videoNotOfTypeTrailer,
  ],
};

describe('Trailer', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should not render without props', () => {
    const { container } = render(<Trailer />);
    expect(container.firstChild).toBeNull();
  });

  it('should render correclty with all props, selecting the first video of type Trailer', () => {
    const { container } = render(<Trailer {...props} />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('should not render when no trailer is passed in video sources', () => {
    const localProps = { results: [videoNotOfTypeTrailer] };
    const { container } = render(<Trailer {...localProps} />);
    expect(container.firstChild).toBeNull();
  });
});
