import classnames from 'classnames';

import './Button.scss';

export type ButtonProps = {
  label?: string;
  isPrimary?: boolean; // Is this the principal call to action on the page?
  size?: 'small' | 'large'; // Make the button smaller/larger
  type?: 'button' | 'submit';
  onClick?: () => void;
  className?: string;
};

/**
 * A button component that can be size adjusted and set to be the primary call to action on the page.
 *
 * @example <Button label="SUBSCRIBE NOW!" isPrimary size="large" />
 */
const Button = ({
  isPrimary = false,
  size,
  type = 'button',
  className = '',
  label,
  onClick,
}: ButtonProps) =>
  label === undefined ? null : (
    <button
      type={type}
      className={classnames('Button', className, {
        'Button--primary': isPrimary,
        'Button--small': size === 'small',
        'Button--large': size === 'large',
      })}
      onClick={onClick}
    >
      {label}
    </button>
  );

export default Button;
