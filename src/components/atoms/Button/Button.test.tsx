import { render, screen, fireEvent } from '@testing-library/react';
import Button, { ButtonProps } from './Button';

const mockOnClick = jest.fn();

const props: ButtonProps = {
  label: 'John Carpenter',
  isPrimary: true,
  size: 'small',
  type: 'button',
  className: 'prop-classname',
  onClick: mockOnClick,
};

describe('Button', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should not render without props', () => {
    const { container } = render(<Button />);
    expect(container.firstChild).toBeNull();
  });

  it('should render correctly with props', () => {
    const { container } = render(<Button {...props} />);
    expect(container.firstChild).toMatchSnapshot();
    expect(mockOnClick).not.toHaveBeenCalled();
  });

  it('should trigger onClick method when Badge is clicked', () => {
    render(<Button {...props} />);
    fireEvent.click(screen.getByRole('button'));
    expect(mockOnClick).toHaveBeenCalledTimes(1);
  });
});
