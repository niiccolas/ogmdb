import { useState, useEffect, useMemo, SyntheticEvent } from 'react';
import classnames from 'classnames';
import { File } from 'react-kawaii';
import { getImgSrcSet } from '../../../utils';
import useVisible from '../../../utils/hooks/useVisible';

import './ResponsiveImage.scss';

enum TmdbImageTypes {
  backdrop = 'backdrop',
  poster = 'poster',
}

export type ResponsiveImageProps = {
  backdropPath?: string;
  posterPath?: string;
  alt?: string;
  displayMode: keyof typeof TmdbImageTypes;
};

/**
 * Responsive Image component with lazy-loading
 */
const ResponsiveImage = ({
  backdropPath,
  posterPath,
  alt,
  displayMode,
}: ResponsiveImageProps) => {
  const { isVisible, ref } = useVisible({
    threshold: 0.3, // 30% image on screen threshold to easily demo lazyloading
    stayVisible: true, // keep visibility to true once image appeared on screen
  });
  const [imageError, setImageError] = useState<SyntheticEvent>();
  const isBackdrop = displayMode === TmdbImageTypes.backdrop;

  /**
   * Memoize to avoid re-computing the somewhat expensive full srcSet when props don't change
   */
  const { lowResSrc, srcSet } = useMemo(() => {
    return getImgSrcSet(displayMode, isBackdrop, backdropPath, posterPath);
  }, [displayMode, isBackdrop, backdropPath, posterPath]);

  /**
   * Progressive enhancement:
   * 1. Start with low-res src "placeholder" to be served/displayed ASAP
   * 2. When it is visible on screen, populate the image's srcset attribute with high-res reponsive assets
   */
  useEffect(() => {
    if (isVisible && !ref.current?.getAttribute('srcset')) {
      ref.current?.setAttribute('srcset', srcSet?.join(', '));
    }
  }, [isVisible, ref, srcSet]);

  return (
    <div
      className={classnames('ResponsiveImage__wrapper', {
        'is-backdrop': isBackdrop,
      })}
    >
      {!imageError ? (
        <img
          ref={ref}
          src={lowResSrc}
          alt={alt}
          className={classnames('ResponsiveImage', {
            'is-backdrop': isBackdrop,
            'is-onscreen': isVisible,
          })}
          onError={setImageError}
        />
      ) : (
        <File mood="ko" />
      )}
    </div>
  );
};

export default ResponsiveImage;
