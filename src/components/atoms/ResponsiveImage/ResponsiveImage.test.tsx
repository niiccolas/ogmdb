import { render, fireEvent, screen } from '@testing-library/react';

import ResponsiveImage, { ResponsiveImageProps } from './ResponsiveImage';

jest.mock('react-kawaii', () => ({
  __esModule: true,
  File: () => <svg>'mock-react-kawaii-File'</svg>,
}));
jest.mock('../../../utils', () => ({
  getImgSrcSet: () => ({
    __esModule: true,
    lowResSrc: 'https://image.tmdb.org/t/p/w300/_mock_.jpg',
    srcSet: [
      'https://image.tmdb.org/t/p/w300/_mock_.jpg 300w',
      'https://image.tmdb.org/t/p/w780/_mock_.jpg 780w',
      'https://image.tmdb.org/t/p/w1280/_mock_.jpg 1280w',
      'https://image.tmdb.org/t/p/original/_mock_.jpg 3840w',
    ],
  }),
}));
const mockUseVisible = jest.fn();
jest.mock('../../../utils/hooks/useVisible', () => () => mockUseVisible());

const props: ResponsiveImageProps = {
  alt: 'Image Alt text',
  backdropPath: '/backdrop.jpg',
  posterPath: '/poster.jpg',
  displayMode: 'poster',
};
const ref = { current: 'mock-ref' };

describe('ResponsiveImage', () => {
  beforeEach(() => {
    jest.clearAllMocks();
    mockUseVisible.mockReturnValue({ isVisible: false, ref });
  });

  it('should render correctly without props', () => {
    // @ts-ignore
    const { container } = render(<ResponsiveImage />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('should render correctly with props', () => {
    const { container } = render(<ResponsiveImage {...props} />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('should render the full srcset on top of the src when image becomes visible', () => {
    mockUseVisible.mockReturnValueOnce({ isVisible: true, ref });
    const { container } = render(<ResponsiveImage {...props} />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('should render a fallback error SVG when loading image fails', () => {
    const { container } = render(<ResponsiveImage {...props} />);
    fireEvent.error(screen.getByAltText('Image Alt text'), Error('mock error'));
    expect(container.firstChild).toMatchSnapshot();
  });
});
