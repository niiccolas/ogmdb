import { render } from '@testing-library/react';

import Spinner from './Spinner';

const props = {
  label: 'mock label',
  className: 'mock classname',
};

describe('Spinner', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should render correctly without props', () => {
    const { container } = render(<Spinner />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('should render correctly with props', () => {
    const { container } = render(<Spinner {...props} />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
