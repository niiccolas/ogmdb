import classnames from 'classnames';

import './Spinner.scss';

type SpinnerProps = {
  label?: string;
  className?: string;
};

const Spinner = ({ label, className }: SpinnerProps) => (
  <>
    <div className={classnames('Spinner', className)}>
      <div className="Spinner__animation" />
      {label && <p className="Spinner__label">{label}</p>}
    </div>
    <div className={classnames('Spinner', className)} />
  </>
);

export default Spinner;
