import { render, screen } from '@testing-library/react';

import Avatar, { AvatarProps } from './Avatar';

jest.mock('randomcolor', () => () => '#EB4A93');

const props: AvatarProps = {
  size: 'small',
  showTooltip: false,
  label: 'Clint Eastwood',
  subLabel: 'as Dirty Harry',
  linkUrl: 'https://url.html',
  imageUrl: '/123.jpg',
  className: 'Avatar--ui-altering-class',
};

describe('Avatar', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it.only('should render correctly without props', () => {
    const { container } = render(<Avatar />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it.only('should render correctly with props', () => {
    const { container } = render(<Avatar {...props} />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it.only('should display label & subLabel on hover when contributed & showTooltip is true', () => {
    render(<Avatar {...props} showTooltip />);
    expect(
      screen.getByLabelText('Avatar image for: Clint Eastwood').firstChild
    ).toMatchSnapshot();
  });
});
