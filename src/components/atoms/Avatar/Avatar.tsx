import classnames from 'classnames';
import randomcolor from 'randomcolor';

import ConditionalWrapper from '../ConditionalWrapper/ConditionalWrapper';
import { formatInitials } from '../../../utils';

import './Avatar.scss';

export type AvatarProps = {
  size?: 'small' | 'large'; // Quick size override
  showTooltip?: boolean; // When true, will display label/sublabel as a tooltip on hover
  label?: string;
  subLabel?: string;
  linkUrl?: string;
  imageUrl?: string;
  className?: string;
};

/**
 * Displays a rounded Avatar component displaying provided `linkUrl` and an optional tooltip on hover. Defaults to a random color when url is missing.
 */
const Avatar = ({
  size,
  className,
  showTooltip = true,
  label,
  linkUrl,
  imageUrl,
  subLabel,
}: AvatarProps) => {
  const bgStyle = !imageUrl
    ? { backgroundColor: randomcolor({ luminosity: 'light' }) }
    : {
        backgroundImage: `url(${imageUrl})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
      };

  return (
    <ConditionalWrapper
      shouldWrap={linkUrl !== undefined}
      wrapper={(children) => <a href={linkUrl}>{children}</a>}
    >
      <div
        className={classnames('Avatar', className, {
          'Avatar--linked': linkUrl,
          'Avatar--large': size === 'large',
          'Avatar--small': size === 'small',
        })}
        style={bgStyle}
        aria-label={label ? `Avatar image for: ${label}` : 'Avatar'}
        role="img"
      >
        {showTooltip && label && (
          <span className="Avatar__label--tooltip" role="contentinfo">
            {label}{' '}
            {subLabel && <span className="Avatar__sublabel">{subLabel}</span>}
          </span>
        )}
        {!imageUrl && (
          <span className="Avatar__imgFallbackLabel">
            {formatInitials(label)}
          </span>
        )}
      </div>
    </ConditionalWrapper>
  );
};

export default Avatar;
