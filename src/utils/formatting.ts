import { TMDB, UNICODE } from './index';

/**
 * Generate uppercase initials for a given string.
 * Sends input to String.normalize() to avoid accented characters
 *
 */
export const formatInitials = (name: string = ''): string =>
  (
    name
      .normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '')
      .match(/\b\w/g) || []
  )
    .map((letter) => letter.toUpperCase())
    .join('');

/**
 * Validates the length of a string based on a minimum threshold
 */
export const isValidQuery = (query: string = '') =>
  query.length >= TMDB.SEARCH_QUERY_MIN_LENGTH;

/**
 * Formats a duration in minutes to a duration expressed in hours and minutes
 * @example formatDuration(95) // '1h35'
 */
export const parseToHoursMinutes = (minutes?: number) => {
  if (!minutes) return undefined;
  const hours = Math.floor(minutes / 60);
  const minutesRemaining = ('0' + (minutes % 60)).slice(-2); // Pad single digits with "0"
  return `${hours}h${minutesRemaining}`;
};

/**
 * Returns the srcset width descriptor of a TMDB image size
 */
const getSrcSetWidthDescriptor = (tmdbImgSize: string) =>
  tmdbImgSize.slice(1) + 'w';

type ReduceReturnType = {
  lowResSrc?: string;
  srcSet: string[];
};

/**
 * Returns a full srcset with width descriptor of a TMDB image size
 * @link https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images
 */
export const getImgSrcSet = (
  displayMode: 'backdrop' | 'poster',
  isBackdrop: boolean,
  backdropPath?: string,
  posterPath?: string
): ReduceReturnType =>
  Object.keys(TMDB.IMG.width[displayMode]).reduce<ReduceReturnType>(
    (acc, width, idx) => {
      const imgIntrinsicSize =
        width === TMDB.IMG.width.original
          ? TMDB.IMG.width[displayMode].original
          : getSrcSetWidthDescriptor(width);

      const imgFullURL =
        TMDB.IMG.baseURL + width + (isBackdrop ? backdropPath : posterPath);

      const fullImgSet = imgFullURL + UNICODE.SPACE + imgIntrinsicSize;

      return idx === 0
        ? // The first image width contains the lowest resolution image: send it in `lowResSrc` key for convenience
          { lowResSrc: imgFullURL, srcSet: [fullImgSet] }
        : // Otherwise add remaining image widths to srcset
          { ...acc, srcSet: [...acc.srcSet, fullImgSet] };
    },
    {} as ReduceReturnType
  );
