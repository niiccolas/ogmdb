import { useRef, useState, useEffect, RefObject } from 'react';

/**
 * Custom hook that returns a `ref` to monitor a DOM element's visibility (`isVisible`)
 * in relation to the top level document viewport or ancestor element.
 *
 * @param options Object of Intersection Observer options:
 * @param [options.root=document] - The reference frame into which intersection
 * will be observed, by default: the top level document viewport
 * @param [options.rootMargin='0px'] - CSS-like margin around the root, e.g.: `"10px 20px 30px 40px"`,
 * **ALL VALUES MUST BE IN `px`**
 * @param [options.threshold=0] - Percentage from 0 to 1 that should be visible
 * before element is considered visible. Can also be an array of numbers, to create multiple trigger points
 */
const useVisible = (
  options: {
    root?: Element | null;
    rootMargin?: string;
    threshold?: number | number[];
    stayVisible?: boolean;
  } = {}
): {
  ref: RefObject<any>;
  isVisible: boolean;
} => {
  const ref = useRef<Element>();
  const [isVisible, setIsVisible] = useState<boolean>(false);
  const stayVisible = useRef<boolean>(options?.stayVisible || false);

  useEffect(() => {
    if (!ref.current) {
      // When the ref has been removed from a DOM element, reset visibility to false
      setIsVisible(false);
      return;
    }

    const observer = new IntersectionObserver(([entry1]) => {
      // If the element is already visible, and the user wants it to stay visible, do nothing
      if (isVisible && stayVisible.current) return;
      setIsVisible(entry1.isIntersecting);

      // If the element is not yet visible, check its visibility (if it intersects with the `root`)
      if (!isVisible) setIsVisible(entry1.isIntersecting);
    }, options);

    // If the ref is set on an element, observe it
    const element = ref.current;
    if (element) observer.observe(element);

    // Unobserve that element on cleanup
    return () => {
      if (element) observer.unobserve(element);
    };
  }, [ref, options, isVisible]);

  return { ref, isVisible };
};

export default useVisible;
