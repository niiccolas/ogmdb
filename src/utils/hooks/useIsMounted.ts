import { useRef, useEffect, RefObject } from 'react';

/**
 * Hook to track a component's mounting state. Useful to avoid setting state on an unmounted component.
 * @returns RefObject - a React ref object
 */
const useIsMounted = (): RefObject<boolean> => {
  const isMounted = useRef(false);

  useEffect(() => {
    isMounted.current = true;
    return () => {
      isMounted.current = false;
    };
  }, []);

  return isMounted;
};

export default useIsMounted;
