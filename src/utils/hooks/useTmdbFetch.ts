import { useState, useEffect, useRef } from 'react';
import { MovieDb } from 'moviedb-promise';
import useIsMounted from './useIsMounted';
import {
  IdAppendToResponseRequest,
  IdRequestParams,
  ListsDetailResponse,
  MovieResponse,
  MovieResultsResponse,
  SearchMovieRequest,
} from 'moviedb-promise/dist/request-types';

export const tmdb = new MovieDb(process.env.REACT_APP_TMDB_API_KEY);

const AXIOS_REQ_CONFIG = {
  timeout: 15000, // 15 seconds timeout opinionated default
};

type EndpointParamsTypes = {
  [key: string]:
    | string
    | number
    | IdAppendToResponseRequest
    | IdRequestParams
    | SearchMovieRequest;
};

/**
 * A custom Hook to avoid duplicating code when querying TMDB. Accepts a boolean `shouldFetch` in the first object argument to conditionally call the API. Prevents from setting state on an unmounted component.
 */
const useTmdbFetch = ({
  endpoint,
  params,
  shouldFetch = true,
}: {
  endpoint: 'listInfo' | 'searchMovie' | 'movieInfo';
  params?: EndpointParamsTypes;
  shouldFetch?: boolean;
}) => {
  const paramsRef = useRef<any>(params);
  const isMounted = useIsMounted();

  const [data, setData] = useState<
    ListsDetailResponse | MovieResultsResponse | MovieResponse
  >();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<Error>();

  useEffect(() => {
    if (shouldFetch) {
      setIsLoading(true);
      setError(undefined);

      tmdb[endpoint](paramsRef?.current, AXIOS_REQ_CONFIG)
        .then((response) => {
          if (isMounted) {
            setData(response);
            setIsLoading(false);
          }
        })
        .catch((e) => {
          if (isMounted) {
            setError(e);
            setIsLoading(false);
          }
        });
    }
  }, [endpoint, isMounted, setData, shouldFetch]);

  return { isLoading, error, data };
};

export default useTmdbFetch;
