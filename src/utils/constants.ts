export enum UNICODE {
  NO_BREAK_SPACE = '\u00a0',
  SPACE = '\u0020',
}

export const TMDB = Object.freeze({
  VIDEO_TYPE: { trailer: 'Trailer' },
  SEARCH_QUERY_MIN_LENGTH: 2,
  /* Sizes based on:
  - https://developers.themoviedb.org/3/configuration/get-api-configuration
  - https://www.themoviedb.org/talk/53c11d4ec3a3684cf4006400
  */
  IMG: {
    baseURL: 'https://image.tmdb.org/t/p/',
    width: {
      original: 'original',
      backdrop: {
        w300: 'w300',
        w780: 'w780',
        w1280: 'w1280',
        original: '3840w',
      },
      logo: {
        w45: 'w45',
        w92: 'w92',
        w154: 'w154',
        w185: 'w185',
        w300: 'w300',
        w500: 'w500',
      },
      poster: {
        w92: 'w92',
        w154: 'w154',
        w185: 'w185',
        w342: 'w342',
        w500: 'w500',
        w780: 'w780',
        original: '2000w',
      },
      profile: {
        w45: 'w45',
        w185: 'w185',
        h632: 'h632',
      },
      still: {
        w92: 'w92',
        w185: 'w185',
        w300: 'w300',
      },
    },
  },
});

export enum TmdbEndpoint {
  LIST_INFO = 'listInfo',
  SEARCH_MOVIE = 'searchMovie',
  MOVIE_INFO = 'movieInfo',
}

export const MOCK_CMS = Object.freeze({
  slices: {
    MOVIE_COLLECTIONS: [
      {
        collectionName: 'Sofia Coppola',
        endpoint: TmdbEndpoint.LIST_INFO,
        params: { id: '8201422' },
      },
      {
        collectionName: 'Ava DuVernay',
        endpoint: TmdbEndpoint.LIST_INFO,
        params: { id: '8201421' },
      },
      {
        collectionName: 'Martin Scorcese',
        endpoint: TmdbEndpoint.LIST_INFO,
        params: { id: '8200604' },
      },
    ],
  },
  i18n: {
    errGenericLabel: 'Something went wrong, please try again later.',
    noDataLabel: `No one's here, retry!`,
    cancel: 'Cancel',
    close: 'Close',
    back: 'Back',
    cast: 'Cast',
    returnList: 'Return to list',
    inputPlaceholder: 'Enter movie title...',
  },
});
